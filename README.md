PureMachine PHP SDK Sample (without framework or composer)
==========================================================

This repository contains a working example of PureBilling
Billing platform using PHP.

This example demonstrates how to use PureBilling SDK in
a PHP project without any framework and composer.

To use it, first clone it and run

```php
sh initialize.sh
sh run-test.php
```


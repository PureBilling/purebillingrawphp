#!/bin/sh
git clone git@github.com:PureBilling/PureBillingSDK.git lib/PureBillingSDK
cd lib/PureBillingSDK
git fetch origin
git checkout master
git reset --hard origin/master
php composer.phar update

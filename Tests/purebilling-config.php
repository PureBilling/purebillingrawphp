<?php

//Autoload the libraries
require_once(__DIR__.'/../lib/PureBillingSDK/autoloader.php');

\PureBilling::setPrivateKey('testprivatekey_0a8972a42750cc808b494a1a5ccdb0b3ef17b36a9a9cdbcc');
\PureBilling::setEndPoint('https://api.purebilling.com');

//Autoload tests namespaces
$autoloader = new SplClassLoader_PureBillingRawPHP('Tests', __DIR__); //Register Tests namespace as this file dir
$autoloader->register();

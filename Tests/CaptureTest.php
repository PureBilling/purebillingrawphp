<?php
namespace Tests;

require_once(__DIR__.'/purebilling-config.php');

use PureBilling\Bundle\SDKBundle\Store\V1 as Store;
use PureMachine\Bundle\SDKBundle\Service\WebServiceClient;
use PureMachine\Bundle\SDKBundle\Exception\WebServiceException;
use PureBilling\Bundle\SDKBundle\Store\V1\Customer\NewCustomer;
use PureBilling\Bundle\SDKBundle\Store\V1\Charge\BillingTransaction;

/**
 * @code
 * phpunit -c Tests/ Tests/Billing/Capture/CaptureTest.php
 * @endcode
 */
class CaptureTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @code
     * phpunit -v --filter testStandardCapture -c Tests/ Tests/Billing/Capture/CaptureTest.php
     * @endcode
     */
    public function testStandardCapture()
    {
        //Creating the customer
        $newCustomer = new NewCustomer();
        $newCustomer->setEmail(uniqid().'@lenon.com');

        //make the call to the PureBilling platform
        $client = new WebServiceClient();
        $response = $client->call('PureBilling/Customer/Create', $newCustomer);

        //Raise an exception if something wrong
        $this->assertEquals('success', $response->getStatus());
        WebServiceException::raiseIfError($response);

        $newCreditcard= new Store\PaymentMethod\NewCreditcard();
        $newCreditcard->setNumber('4111111111111111');
        $newCreditcard->setExpirationMonth(1);
        $newCreditcard->setExpirationYear(2030);
        $newCreditcard->setCvv(123);
        $newCreditcard->setCardOwner('Albert John');

        //Create the capture object
        $capture = new Store\Charge\Action\Capture();
        $capture->setAmount(5.25);
        $capture->setPaymentMethod($newCreditcard);
        $capture->setCountry('FR');
        $capture->setCurrency('EUR');
        $capture->setCustomer($response->getAnswer()->getId()); #FIXME: Capture should be possible without customer

        //make the call to the PureBilling platform
        $responseCapture = $client->call('PureBilling/Charge/Capture', $capture);
        WebServiceException::raiseIfError($responseCapture);

        $this->assertEquals('success', $responseCapture->getStatus());
        $answer = $responseCapture->getAnswer();
        $this->assertTrue(is_array($answer));
        $this->assertCount(1, $answer);
        $firstBilling = $answer[0];
        $this->assertTrue($firstBilling instanceof BillingTransaction);
        $this->assertEquals('collected', $firstBilling->getDetailledStatus());
        $this->assertEquals(5.25, $firstBilling->getAmount());
    }

}
